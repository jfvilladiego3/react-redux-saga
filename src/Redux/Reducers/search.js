import { get } from 'lodash'
import {
  SEARCH_MOVIE_START,
  SEARCH_MOVIE_ERROR,
  SEARCH_MEDIA_COMPLETE
  } from '../../Const/actionTypes'

const initialState = {};

export default function(state = initialState, action){
  switch (action.type){
    case SEARCH_MOVIE_START:
      console.log(action);
      return { ...state }
      break;
    case SEARCH_MOVIE_ERROR:
      console.log(action);
      return { ...state }
      break;
    case SEARCH_MEDIA_COMPLETE:
      console.log(action);
      return { ...state }
      break;
    default:
      return { ...state }
      break;
  }
}