import { SEARCH_MOVIE_START } from '../../Const/actionTypes';

export const searchMovie = payload => ({
  type: SEARCH_MOVIE_START,
  payload
})