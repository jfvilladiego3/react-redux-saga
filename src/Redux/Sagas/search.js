import { put, call, takeLatest } from 'redux-saga/effects';

import {
SEARCH_MOVIE_START,
SEARCH_MOVIE_ERROR,
SEARCH_MEDIA_COMPLETE
} from '../../Const/actionTypes'

import { apiCall } from '../../Services';

export function* searchMovie({ payload }) {
  try {
    console.log(payload.movieName);
    
    const results = yield call(apiCall, `&s=${payload.movieName}`, null, null, 'GET')
    yield put({ type: SEARCH_MEDIA_COMPLETE, results })
    console.log('Inital Action', payload);
  } catch (e) {
    yield put({ type: SEARCH_MOVIE_ERROR, e })
  }
}

export default function* search(params) {
  yield takeLatest(SEARCH_MOVIE_START, searchMovie)
}