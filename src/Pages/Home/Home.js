import React, {useState} from 'react'
import {
  Typography,
  TextField,
  Card,
  Button,
  Container,
  Grid
} from "@material-ui/core";

function Home({ history }) {
  const [searchText, setSearchText] = useState("");

  const handleSearchTextChange = event => {
    setSearchText(event.target.value);
  };

  const handleClearClick = event => {
    setSearchText("");
  };

  const handleSearchClick = event => {
    history.push(`/results/${searchText}`);
  };

  return (
    <div className="">
      <TextField
        value={searchText}
        placeholder="Buscar"
        onChange={handleSearchTextChange}
        margin="normal"
      />
      <br/>
      <Button 
      onClick={handleSearchClick} 
      variant="contained" 
      color="primary">
        Hello World
      </Button>
    </div>
  )
}

export default Home
