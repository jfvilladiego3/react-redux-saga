import React,{useEffect} from 'react';
import { useDispatch } from 'react-redux';

/*Actions*/
import { searchMovie } from '../../Redux/Actions/search'

function Results(props) {
  const {movieName} = props.match.params
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(searchMovie({ movieName }))
    console.log(dispatch);
  },[])

  return (
    <div>
      lists
    </div>
  )
}

export default Results
