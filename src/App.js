import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import './App.css';

import Home from './Pages/Home/Home'
import Results from './Pages/Results/Results'

function App( {store} ) {
  return (
    <Provider store={store}>
      <Router>
        <div>
          <Route exact path="/" component={Home} />
          <Route exact path="/results/:movieName" component={Results} />
        </div>
      </Router>
	</Provider>
  );
}

App.propTypes = {
	store: PropTypes.object.isRequired
};

export default App;
